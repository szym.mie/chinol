WITH to_advance_rooms AS (
         SELECT id, stage FROM rooms WHERE extract(epoch from now() at time zone 'utc') >= time_next
     ),
     to_advance_players AS (
         SELECT session, to_advance_rooms.stage FROM to_advance_rooms, players
                                 LEFT JOIN rooms r1 ON players.session = r1.p1_id
                                 LEFT JOIN rooms r2 ON players.session = r2.p2_id
                                 LEFT JOIN rooms r3 ON players.session = r3.p3_id
                                 LEFT JOIN rooms r4 ON players.session = r4.p4_id
         WHERE coalesce(r1.id, r2.id, r3.id, r4.id) IN (SELECT id FROM to_advance_rooms)
     ),
     advance_players AS (
         UPDATE players SET
             status = CASE (SELECT stage FROM to_advance_players WHERE to_advance_players.session = players.session)
                          WHEN 0 THEN 2
                          WHEN 1 THEN 5
                 END
             WHERE session IN (SELECT session FROM to_advance_players)
     ),
     advance_rooms AS (
         UPDATE rooms SET
             stage = stage + 1,
             time_next = CASE
                 WHEN stage = 0 THEN extract(epoch from now() at time zone 'utc') + 3600
                 WHEN stage = 1 THEN extract(epoch from now() at time zone 'utc') + 300
                 END
             WHERE id IN (SELECT id FROM to_advance_rooms)
     ),
     unsuitable_rooms AS (
         SELECT coalesce(p1.status::bool::int, 0) +
                coalesce(p2.status::bool::int, 0) +
                coalesce(p3.status::bool::int, 0) +
                coalesce(p4.status::bool::int, 0) sum FROM rooms
                                                    LEFT JOIN players p1 on p1.session = rooms.p1_id
                                                    LEFT JOIN players p2 on p2.session = rooms.p2_id
                                                    LEFT JOIN players p3 on p3.session = rooms.p3_id
                                                    LEFT JOIN players p4 on p4.session = rooms.p4_id
     ),
--      active_players_timeout AS (
--          UPDATE players SET
--              status = 4
--          FROM rooms
--          WHERE extract(epoch from now() at time zone 'utc') > player_time
--                    AND session = CASE active_player
--                        WHEN 1 THEN p1_id
--                        WHEN 2 THEN p2_id
--                        WHEN 3 THEN p3_id
--                        WHEN 4 THEN p4_id
--                        END
--      ),
     old_player_session AS (
         SELECT CASE active_player
             WHEN 1 THEN p1_id
             WHEN 2 THEN p2_id
             WHEN 3 THEN p3_id
             WHEN 4 THEN p4_id
             END AS old_session
         FROM rooms
         WHERE extract(epoch from now() at time zone 'utc') > player_time
     ),
     active_timeout_room AS (
         UPDATE rooms SET
             player_throws = 1,
             player_time = DEFAULT,
             active_player = CASE active_player
                                 WHEN 1 THEN CASE WHEN p2_id IS NOT NULL THEN 2 WHEN p3_id IS NOT NULL THEN 3 ELSE 4 END
                                 WHEN 2 THEN CASE WHEN p3_id IS NOT NULL THEN 3 WHEN p4_id IS NOT NULL THEN 4 ELSE 1 END
                                 WHEN 3 THEN CASE WHEN p4_id IS NOT NULL THEN 4 WHEN p1_id IS NOT NULL THEN 1 ELSE 2 END
                                 WHEN 4 THEN CASE WHEN p1_id IS NOT NULL THEN 1 WHEN p2_id IS NOT NULL THEN 2 ELSE 3 END
                 END
             WHERE extract(epoch from now() at time zone 'utc') > player_time
             RETURNING
                 CASE active_player
                     WHEN 1 THEN p1_id
                     WHEN 2 THEN p2_id
                     WHEN 3 THEN p3_id
                     WHEN 4 THEN p4_id
                     END AS active_session
     ),
     new_active_player AS (
         UPDATE players SET
             status = CASE
                          WHEN session IN (SELECT active_session FROM active_timeout_room) THEN 3
                          WHEN session IN (SELECT old_session FROM old_player_session) THEN 4
                          ELSE status
                 END
         FROM rooms
         WHERE (p1_id = players.session OR
                p2_id = players.session OR
                p3_id = players.session OR
                p4_id = players.session) AND stage = 1
     )

DELETE FROM rooms WHERE (SELECT sum FROM unsuitable_rooms) < 2 AND (id IN (SELECT id FROM to_advance_rooms) OR stage > 0);

