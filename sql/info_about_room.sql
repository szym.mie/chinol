-- get room based on session.

WITH room AS (
    SELECT id FROM rooms
    WHERE rooms.p1_id = ?1 OR rooms.p2_id = ?1 OR rooms.p3_id = ?1 OR rooms.p4_id = ?1
    LIMIT 1
)

SELECT rooms.name room_name,
       rooms.stage room_stage,
       rooms.board room_board,
       rooms.active_player room_active_player,
       rooms.time_next room_time_next,
       rooms.player_throws room_player_throws,
       rooms.last_throw room_last_throw,
       rooms.player_time room_player_time,
       p1.name p1_name, p1.status p1_status,
       p2.name p2_name, p2.status p2_status,
       p3.name p3_name, p3.status p3_status,
       p4.name p4_name, p4.status p4_status,
       CASE
           WHEN rooms.p1_id = ?1 THEN 0
           WHEN rooms.p2_id = ?1 THEN 1
           WHEN rooms.p3_id = ?1 THEN 2
           WHEN rooms.p4_id = ?1 THEN 3
           END player_order
FROM room, rooms
    LEFT JOIN players p1 ON p1.session = rooms.p1_id
    LEFT JOIN players p2 ON p2.session = rooms.p2_id
    LEFT JOIN players p3 ON p3.session = rooms.p3_id
    LEFT JOIN players p4 ON p4.session = rooms.p4_id
WHERE rooms.id = room.id;