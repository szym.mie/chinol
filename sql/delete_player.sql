WITH room_update AS (
    UPDATE rooms SET
        p1_id = nullif(p1_id, ?1),
        p2_id = nullif(p2_id, ?1),
        p3_id = nullif(p3_id, ?1),
        p4_id = nullif(p4_id, ?1)
    WHERE p1_id = ?1 OR p2_id = ?1 OR p3_id = ?1 OR p4_id = ?1
)

DELETE FROM players WHERE session = ?1;