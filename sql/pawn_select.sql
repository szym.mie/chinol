WITH board_update AS (
         UPDATE rooms SET
             board[(active_player - 1) * 4 + ?2] = CASE
                WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 1 OR last_throw = 6) THEN (active_player - 1)*11 + 2
                WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 2 OR last_throw = 3 OR last_throw = 4 OR last_throw = 5) THEN 0
                WHEN board[(active_player - 1) * 4 + ?2] = 1 THEN 1
                WHEN board[(active_player - 1) * 4 + ?2] + rooms.last_throw > (active_player - 1)*11 + 45 THEN 1
                ELSE board[(active_player - 1) * 4 + ?2] + rooms.last_throw
                END,
        player_throws = 1,
        player_time = DEFAULT,
        active_player = CASE active_player
                            WHEN 1 THEN CASE WHEN p2_id IS NOT NULL THEN 2 WHEN p3_id IS NOT NULL THEN 3 ELSE 4 END
                            WHEN 2 THEN CASE WHEN p3_id IS NOT NULL THEN 3 WHEN p4_id IS NOT NULL THEN 4 ELSE 1 END
                            WHEN 3 THEN CASE WHEN p4_id IS NOT NULL THEN 4 WHEN p1_id IS NOT NULL THEN 1 ELSE 2 END
                            WHEN 4 THEN CASE WHEN p1_id IS NOT NULL THEN 1 WHEN p2_id IS NOT NULL THEN 2 ELSE 3 END
            END
        WHERE CASE active_player
                  WHEN 1 THEN p1_id = ?1
                  WHEN 2 THEN p2_id = ?1
                  WHEN 3 THEN p3_id = ?1
                  WHEN 4 THEN p4_id = ?1
                  END AND player_throws = 0
        RETURNING CASE active_player
            WHEN 1 THEN p1_id
            WHEN 2 THEN p2_id
            WHEN 3 THEN p3_id
            WHEN 4 THEN p4_id
            END AS active_session
)

UPDATE players SET
    status = CASE
        WHEN session IN (SELECT active_session FROM board_update) THEN 3
        WHEN status = 3 THEN 2
        ELSE status
        END
FROM rooms
WHERE (p1_id = players.session OR
      p2_id = players.session OR
      p3_id = players.session OR
      p4_id = players.session) AND stage = 1;
