-- finds first room that has empty slot.

SELECT id FROM rooms WHERE (p1_id IS NULL OR p2_id IS NULL OR p3_id IS NULL OR p4_id IS NULL) AND stage = 0 LIMIT 1;