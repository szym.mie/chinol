-- finds rooms with names similar to specified.

SELECT id FROM rooms WHERE name LIKE ?1 LIMIT 25;