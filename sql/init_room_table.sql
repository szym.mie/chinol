-- time_next - time of next stage change.

CREATE TABLE IF NOT EXISTS rooms (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    p1_id UUID UNIQUE,
    p2_id UUID UNIQUE,
    p3_id UUID UNIQUE,
    p4_id UUID UNIQUE,
    FOREIGN KEY (p1_id)
        REFERENCES players (session)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    FOREIGN KEY (p2_id)
        REFERENCES players (session)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    FOREIGN KEY (p3_id)
        REFERENCES players (session)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    FOREIGN KEY (p4_id)
        REFERENCES players (session)
        ON DELETE NO ACTION
        ON UPDATE CASCADE,
    stage INTEGER NOT NULL DEFAULT 0,
    time_next INTEGER NOT NULL DEFAULT ( extract(epoch from now() at time zone 'utc') + 180 ),
    board INTEGER ARRAY[16] DEFAULT '{0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0}',
    active_player INTEGER DEFAULT 1,
    player_throws INTEGER DEFAULT 1,
    last_throw INTEGER DEFAULT ( floor(random() * 6 + 1)::int ),
    player_time INTEGER NOT NULL DEFAULT ( extract(epoch from now() at time zone 'utc') + 15 )
);