DELETE FROM rooms
    WHERE p1_id IS NULL
      AND p2_id IS NULL
      AND p3_id IS NULL
      AND p4_id IS NULL
    OR stage > 1;