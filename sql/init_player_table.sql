CREATE TABLE IF NOT EXISTS players (
    session UUID UNIQUE PRIMARY KEY DEFAULT ( gen_random_uuid() ),
    name TEXT NOT NULL,
    status INTEGER NOT NULL DEFAULT 0,
    logon_time INTEGER NOT NULL DEFAULT ( extract(epoch from now() at time zone 'utc') ),
    active_time INTEGER NOT NULL DEFAULT ( extract(epoch from now() at time zone 'utc') )
);