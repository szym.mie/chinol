UPDATE rooms SET
                 last_throw = DEFAULT,
                 player_throws = player_throws - 1
WHERE CASE active_player
    WHEN 1 THEN p1_id = ?1
    WHEN 2 THEN p2_id = ?1
    WHEN 3 THEN p3_id = ?1
    WHEN 4 THEN p4_id = ?1
    END AND player_throws > 0
RETURNING last_throw;