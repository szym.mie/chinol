-- logon SQL.
-- updates selected room.
WITH empty AS (
    SELECT CASE
        WHEN p1_id IS NULL THEN 1
        WHEN p2_id IS NULL THEN 2
        WHEN p3_id IS NULL THEN 3
        WHEN p4_id IS NULL THEN 4
    END AS first
    FROM rooms WHERE id = ?1
)

UPDATE rooms SET
    p1_id = CASE WHEN empty.first = 1 THEN ?2 ELSE p1_id END,
    p2_id = CASE WHEN empty.first = 2 THEN ?2 ELSE p2_id END,
    p3_id = CASE WHEN empty.first = 3 THEN ?2 ELSE p3_id END,
    p4_id = CASE WHEN empty.first = 4 THEN ?2 ELSE p4_id END
    FROM empty
    WHERE id = ?1 AND stage = 0;