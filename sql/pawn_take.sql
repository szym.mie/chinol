WITH pow_find AS (
    SELECT unnest(array_positions(board,
                                  CASE
                                      WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 1 OR last_throw = 6) THEN (active_player - 1)*11 + 2
                                      WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 2 OR last_throw = 3 OR last_throw = 4 OR last_throw = 5) THEN 79
                                      WHEN board[(active_player - 1) * 4 + ?2] = 1 THEN 79
                                      WHEN board[(active_player - 1) * 4 + ?2] + rooms.last_throw > (active_player - 1)*11 + 45 THEN 79
                                      ELSE board[(active_player - 1) * 4 + ?2] + rooms.last_throw
                                      END
        )) AS pows
    FROM rooms
)
UPDATE rooms SET board = array_replace(board,
        CASE
            WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 1 OR last_throw = 6) THEN (active_player - 1)*11 + 2
            WHEN board[(active_player - 1) * 4 + ?2] = 0 AND (last_throw = 2 OR last_throw = 3 OR last_throw = 4 OR last_throw = 5) THEN 79
            WHEN board[(active_player - 1) * 4 + ?2] = 1 THEN 79
            WHEN board[(active_player - 1) * 4 + ?2] + rooms.last_throw > (active_player - 1)*11 + 45 THEN 79
            ELSE board[(active_player - 1) * 4 + ?2] + rooms.last_throw
            END
 , 0)
 FROM pow_find
 WHERE CASE active_player
           WHEN 1 THEN p1_id = ?1
           WHEN 2 THEN p2_id = ?1
           WHEN 3 THEN p3_id = ?1
           WHEN 4 THEN p4_id = ?1
           END AND pows NOT IN (1 + (active_player - 1) * 4, 2 + (active_player - 1) * 4, 3 + (active_player - 1) * 4, 4 + (active_player - 1) * 4);
