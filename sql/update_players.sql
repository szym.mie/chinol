WITH
    expire_players AS ( -- if no sync was sent in about 3 seconds notify this.
        UPDATE players SET status = status + 8
            WHERE status BETWEEN 0 AND 7
                AND extract(epoch from now() at time zone 'utc') - 3 > active_time
    ),
    ressurect_players AS ( -- if back online reflect that.
        UPDATE players SET status = status - 8
            WHERE status BETWEEN 8 AND 15
                AND extract(epoch from now() at time zone 'utc') - 3 < active_time
    ),
    dead_players AS (
        SELECT session FROM players
                                LEFT JOIN rooms r1 ON players.session = r1.p1_id
                                LEFT JOIN rooms r2 ON players.session = r2.p2_id
                                LEFT JOIN rooms r3 ON players.session = r3.p3_id
                                LEFT JOIN rooms r4 ON players.session = r4.p4_id
        WHERE extract(epoch from now() at time zone 'utc') -
              CASE coalesce(r1.stage, r2.stage, r3.stage, r4.stage)
                  WHEN 0 THEN 10
                  WHEN 1 THEN 1800
                  ELSE 10800
                  END > active_time
    ),
    delink_players AS ( -- depending on how much time elapsed you might want to kick them out.
        UPDATE rooms SET
            p1_id = CASE p1_id NOT IN (SELECT dead_players.session FROM dead_players) WHEN TRUE THEN p1_id END,
            p2_id = CASE p2_id NOT IN (SELECT dead_players.session FROM dead_players) WHEN TRUE THEN p2_id END,
            p3_id = CASE p3_id NOT IN (SELECT dead_players.session FROM dead_players) WHEN TRUE THEN p3_id END,
            p4_id = CASE p4_id NOT IN (SELECT dead_players.session FROM dead_players) WHEN TRUE THEN p4_id END
    )

DELETE FROM players WHERE session IN (SELECT dead_players.session FROM dead_players);