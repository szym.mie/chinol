class Cookie {
    static read (cname) {
        const name = cname + "=";
        const tokens = document.cookie.split(";");
        return tokens
            .filter(c => c.includes(name))
            .map(c => decodeURIComponent(c.split("=")[1]))[0];
    }

    static write (name, value) {
        document.cookie = name + "=" + encodeURIComponent(value) + ";path=/";
    }
}

class IdentInfo {
    static read () {
        return {
            session: Cookie.read("session")
        };
    }

    static write (json) {
        Cookie.write("session", json.session);
    }
}

class StageInfo {
    static read () {
        return {
            stage: Cookie.read("stage")
        }
    }

    static write (json) {
        Cookie.write("stage", json.room_stage);
    }
}

export { Cookie, IdentInfo, StageInfo };