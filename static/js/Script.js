class Script {
    static replace (node) {
        if (node.tagName === "SCRIPT")
            node.parentNode.replaceChild(this.clone(node), node);
        else
            for (const child of node.children) this.replace(child);

        return node;
    }

    static clone (node) {
        const s = document.createElement("script");
        s.text = node.innerText;

        for (const attr of node.attributes) s.setAttribute(attr.name, attr.value);

        return s;
    }
}

export { Script };