import { IdentInfo, StageInfo } from "/js/Info.js";
import { Xhr } from "/js/Xhr.js";
import { Script } from "/js/Script.js";
import { SHARED } from "/js/State.js";
import { BarView } from "/js/View.js";

class Methods {
    static ident () {
        Xhr.new(
            "/ident",
            JSON.stringify(IdentInfo.read()),
            res => {
                location.replace("https://szymon-miekina-chin.herokuapp.com" + res.responseText);
            });
    }

    static logon (name) {
        Xhr.new(
            "/logon",
            JSON.stringify({name: name}),
             res => {
                IdentInfo.write(JSON.parse(res.responseText));
                StageInfo.write({room_stage: 0});
                console.log(res.responseText);
                this.ident();
            });
    }

    static sync () {
        Xhr.new(
            "/sync",
            JSON.stringify(IdentInfo.read()),
            res => {
                const data = JSON.parse(res.responseText);
                console.log(data.room_stage, data.room_player_time, data.p1_status, data.p2_status);

                // if (StageInfo.read().stage != data.room_stage) {
                //     SHARED.current_stage = data.room_stage;
                //     StageInfo.write(data);
                //     this.ident();
                // }

                try {
                    SHARED.sync_data = data;
                    const player_bar = document.getElementsByClassName("player_cont")[0];
                    BarView.player_bar(player_bar, data);
                } catch {}
            });
    }

    static rdy (ready=false) {
        Xhr.new(
            "/rdy",
            JSON.stringify(Object.assign(
                IdentInfo.read(),
                {ready: ready}
            )),
            _ => {
                // do nothing really.
            });
    }

    static act (no, cb=()=>{}) {
        Xhr.new(
            "/act",
            JSON.stringify(Object.assign(
                IdentInfo.read(),
                {no: no}
            )),
            res => {
                cb(JSON.parse(res.responseText));
            });
    }

    static end () {
        Xhr.new(
            "/end",
            JSON.stringify(IdentInfo.read()),
            res => {
                document.body.innerText = ""; // clear anything in body.
                document.body.insertAdjacentHTML("afterbegin", res.responseText);
            });
    }
}

export { Methods };