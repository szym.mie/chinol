import { Methods } from "/js/Methods.js";
import { Defs } from "/js/Defs.js";

class Dialog {
    static logon (ev) {
        const name = ev.value;
        console.log(name);
        if (name !== "") {
            Methods.logon(name);
        }
    }

    static logoff (ev) {
        const text = document.getElementById("logoff").children[1];
        if (text.style.display === "none") {
            text.style.display = "block";
            setTimeout(() => { text.style.display = "none" }, 1500);
        } else {
            Methods.end();
        }
    }

    static ready (ev) {
        Methods.rdy(ev.target.checked);
    }
}

class BarView {
    static status_symbol (status) {
        switch (status) {
            case 0: return "/ready_not_symbol.svg";
            case 1: return "/ready_ok_symbol.svg";
            case 2: return "/wait_symbol.svg";
            case 3: return "/roll_symbol.svg";
            case 4: return "/away_symbol.svg";
            case 8:
            case 9:
            case 10:
            case 11:
            case 12: return "/desync_symbol.svg";
            default: return "/empty_symbol.svg";
        }
    }

    static player_select (data, index) {
        switch (index) {
            case 1: return { name: data.p1_name, status: data.p1_status };
            case 2: return { name: data.p2_name, status: data.p2_status };
            case 3: return { name: data.p3_name, status: data.p3_status };
            case 4: return { name: data.p4_name, status: data.p4_status };
            default: return {};
        }
    }

    static time_print (seconds) {
        const min = Math.floor(seconds/60).toString().padStart(2, '0');
        const sec = Math.floor(seconds%60).toString().padStart(2, '0');
        return seconds >= 0 ? (min + ':' + sec) : "--:--";
    }

    static player_bar (bar_root, data) {
        const children = [...bar_root.children];
        const fields = children.filter(c => c.className === "player_field");
        const timer = children.filter(c => c.className === "timer_cont")[0];

        for (let i = 0; i < 4; i++) {
            const field = fields[i].children;
            const player = this.player_select(data, i+1);

            field[0].innerText = Defs.nil(player) ? "-" : player.name;
            field[1].src = this.status_symbol(player.status);
        }

        timer.children[1].innerText = this.time_print(data.room_time_next - Math.floor(Date.now() / 1000));
    }
}

export { Dialog, BarView };