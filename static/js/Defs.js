class Defs {
    static nil (n) { return [null, undefined].includes(n); }
}

export { Defs };