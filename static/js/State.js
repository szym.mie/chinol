class SHARED {
    static current_stage = 0;
    static sync_interval = null;
    static sync_data = {};
}

export { SHARED };