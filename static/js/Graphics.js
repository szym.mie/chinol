import { SHARED } from "/js/State.js";
import { Methods } from "/js/Methods.js";
import { Speak } from "/js/Speak.js";

class BoardPattern {
    constructor (pattern=[]) {
        this.board = new Int8Array(169);
        this.board.fill(0);

        const m = Math.min(pattern.length, 169)
        for (let i = 0; i < m; i++) this.board[i] = pattern[i];
    }

    display () {
        let str = "";
        let i = 0
        while (i < 13) str += (this.board.slice(i*13, (++i)*13) + '\n');

        console.log(str);
    }
}

class Renderer {
    constructor () {
        this.canvas = null;
        this.ctx = null;
        this.pattern = new BoardPattern();

        this.widths = {};

        this.colors = {
            bg: "#bfb1b2",
            fg: "#1f1a19",
            c1: "#db242f",
            c2: "#2348d0",
            c3: "#dba011",
            c4: "#38a509"
        }

        this.ctable = [
            this.colors.bg,
            this.colors.fg,
            this.colors.c1,
            this.colors.c2,
            this.colors.c3,
            this.colors.c4
        ];

        this.start_pos = new Uint8Array([
            14, 15, // red
            27, 28,
            23, 24, // blue
            36, 37,
            140, 141, // yellow
            153, 154,
            131, 132, // green
            144, 145
        ]);

        this.end_pos = new Uint8Array([
            79, 80, 81, 82, // red
            19, 32, 45, 58, // blue
            86, 87, 88, 89, // yellow
            110, 123, 136, 149 // green
        ]);

        this.board_pos = new Uint8Array([
            66, 67, 68, 69, 57, 44, 31, 18, 5, 6, 7, // red
            20, 33, 46, 59, 73, 74, 75, 76, 77, 90, 103, // blue
            102, 101, 100, 99, 111, 124, 137, 150, 163, 162, 161, // yellow
            148, 135, 122, 109, 95, 94, 93, 92, 91, 78, 65 // green
        ]);

        this.dice = {
            sT: 0,
            t_rot: 0,
            val: 1,
            pattern: {
                1: new Float32Array([
                    .5, .5]),
                2: new Float32Array([
                    .3, .3,
                    .7, .7]),
                3: new Float32Array([
                    .25, .25,
                    .5, .5,
                    .75, .75]),
                4: new Float32Array([
                    .3, .3,
                    .3, .7,
                    .7, .7,
                    .7, .3]),
                5: new Float32Array([
                    .25, .25,
                    .25, .75,
                    .75, .75,
                    .75, .25,
                    .5, .5]),
                6: new Float32Array([
                    .3, .25,
                    .3, .5,
                    .3, .75,
                    .7, .25,
                    .7, .5,
                    .7, .75])
            }
        }

        this.mode = 0;

        this.T = 0;
    }

    clear_screen () {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    itoc (i, o=false) {
        const w = this.widths.square;
        const {x: cx, y: cy} = this.widths.center;
        const h = w/2;
        const x = i % 13;
        const y = i / 13 | 0;
        return o ? [x*w+h+cx, y*w+h+cy] : [x*w+cx , y*w+cy];
    }

    ctoi (x, y) {
        const w = this.widths.square;
        const rx = (x - this.widths.center.x);
        const ry = (y - this.widths.center.y);
        const u = rx / w | 0;
        const v = ry / w | 0;
        return rx >= 0 && ry >= 0 && u < 13 && v < 13 ? u + v*13 : -1;
    }

    draw_line (x1, y1, x2, y2) {
        this.ctx.moveTo(x1, y1);
        this.ctx.lineTo(x2, y2);
    }

    draw_circle (x, y, r, s=8) {
        this.ctx.beginPath();

        const o = Math.PI*2/s;

        this.ctx.moveTo(x, y + r);

        for (let i = 1; i < s; i++) this.ctx.lineTo(x + Math.sin(i*o) * r, y + Math.cos(i*o) * r);

        this.ctx.closePath();
    }

    draw_squares () {
        this.ctx.save();

        const w = this.widths.square;

        for (let i = 0; i < 169; i++) {
            const [x, y] = this.itoc(i);
            const t = this.pattern.board[i];
            this.ctx.fillStyle = this.ctable[t];

            this.ctx.fillRect(x, y, w, w);
        }

        this.ctx.restore();
    }

    draw_circles () {
        this.ctx.save();

        this.ctx.strokeStyle = this.colors.fg;

        const w = this.widths.border;

        const draw = (this.T/500 | 0) % 2;

        const last = [];

        for (let i = 0; i < 16; i++) {
            const c = (i / 4 | 0);

            const ind = SHARED.sync_data.room_board[i];

            const pos =
                ind === 0 ? this.start_pos[i] :
                ind === 1 ? this.end_pos[i] :
                    this.board_pos[(ind-2)%44];

            const stack = last.filter(old => old === pos).length;
            last.push(pos);

            const [x, y] = this.itoc(pos, true);

            console.log(SHARED.sync_data.room_active_player, SHARED.sync_data.player_order);

            if (i % 4 === 0) {
                if (!(this.mode === 1 &&
                    SHARED.sync_data.room_active_player-1 === SHARED.sync_data.player_order &&
                    SHARED.sync_data.player_order === c)) {
                    this.ctx.fillStyle = this.ctable[c + 2];
                } else {
                    this.ctx.fillStyle = draw ? this.colors.bg : this.ctable[c + 2];
                }
            }

            this.draw_circle(x + w*stack, y-w*stack, this.widths.circle);

            this.ctx.stroke();
            this.ctx.fill();
        }

        this.ctx.restore();
    }

    draw_borders () {
        this.ctx.save();

        this.ctx.strokeWidth = this.widths.border;
        this.ctx.strokeStyle = this.colors.fg;

        const w = this.widths.square;

        const x1 = this.widths.center.x
        const x2 = this.widths.center.x + w*13;

        const y1 = this.widths.center.y
        const y2 = this.widths.center.y + w*13;

        for (let l = 0; l < 14; l++) {
            const o = w * l;
            const yo = y1 + o;
            const xo = x1 + o;

            this.draw_line(x1, yo, x2, yo); // horizontal
            this.draw_line(xo, y1, xo, y2); // vertical
        }

        this.ctx.stroke();
        this.ctx.restore();
    }

    anim_dice (thrown) {
        this.dice.sT = this.T;

        this.dice.rot = Math.random()*2;

        this.dice.val = thrown;
        Speak.speak("wyrzucono" + thrown);
    }

    draw_dice () {
        if (this.mode === 1) {
            this.ctx.save();

            this.ctx.fillStyle = this.colors.bg;

            const cx = this.widths.square * 5.5 + this.widths.center.x;
            const cy = this.widths.square * 5.5 + this.widths.center.y;

            const sw = this.widths.square * 2;
            const bw = this.widths.border * 2;

            this.ctx.fillRect(cx, cy, sw, sw);

            this.ctx.fillStyle = this.colors.fg;

            const t = this.dice.val;
            const p = this.dice.pattern[t];
            for (let i = 0; i < t; i++) {
                const j = i * 2;
                const x = sw * p[j];
                const y = sw * p[j + 1];
                this.draw_circle(cx + x, cy + y, bw, 8);
                this.ctx.fill();
            }

            this.ctx.restore();
        }
    }

    change_canvas (canvas) {
        if (this.canvas !== null) this.canvas.removeEventListener("click", this.click_action);
        this.canvas = canvas;
        this.canvas.addEventListener("click", this.click_action.bind(this));

        this.ctx = this.canvas.getContext("2d");

        this.viewport();
    }

    click_action (ev) {
        const ex = ev.offsetX-1;
        const ey = ev.offsetY-1;

        switch (this.mode) {
            case 0:
                this.dice_throw();
                break;
            case 1:
                this.pawn_select(ex, ey);
                break;
            default:
                break;
        }
    }

    dice_throw () {
        console.log("dice thrown");
        Methods.act(0, json => {
            if (json.status === "ok") {
                this.mode = 1;
                console.log(json.thrown);
                this.anim_dice(json.thrown);
            }
        });
    }

    pawn_select (ex, ey) {
        const player = SHARED.sync_data.player_order;

        if (SHARED.sync_data.room_active_player-1 !== SHARED.sync_data.player_order) this.mode = 0;

        console.log(player);
        const cli = this.ctoi(ex, ey);

        for (let i = player*4; i < (player+1)*4; i++) {
            const ind = SHARED.sync_data.room_board[i];

            const pos =
                ind === 0 ? this.start_pos[i] :
                ind === 1 ? this.end_pos[i] :
                    this.board_pos[(ind-2)%44];

            console.log(cli, pos);
            if (cli === pos) {
                Methods.act(1 + i%4);
                this.mode = 0;
                console.log("pawn selected: " + (1 + i));
            }
        }
    }

    viewport () {
        const min_dim = Math.min(this.canvas.width, this.canvas.height);

        this.widths.square = Math.floor(min_dim/130)*10;
        this.widths.circle = this.widths.square/10*4;
        this.widths.border = this.widths.square/10;
        this.widths.center = {
            x: Math.floor((this.canvas.width - this.widths.square*13) / 2),
            y: Math.floor((this.canvas.height - this.widths.square*13) / 2)
        };
    }

    main (T=0) {
        this.T = T;


        if (SHARED.sync_data.room_board !== undefined) {
            this.clear_screen();

            this.draw_squares();
            this.draw_borders();
            this.draw_circles();
            this.draw_dice();
        }

        requestAnimationFrame(this.main.bind(this));
    }
}

export { BoardPattern, Renderer };