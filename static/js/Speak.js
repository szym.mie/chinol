class Speak {
    static voices = speechSynthesis.getVoices();
    static voice = this.voices.some(({lang: l}) => l === "pl-PL") ?
        this.voices.find(({lang: l}) => l === "pl-PL") : this.voices[0];

    static speak (text) {
        const u = new SpeechSynthesisUtterance(text);
        if (this.voice !== undefined) u.voice = this.voice;

        speechSynthesis.speak(u);
    }
}

export { Speak };