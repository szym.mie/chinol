class Xhr {
    static new (url, data, fn) {
        const xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        xhr.overrideMimeType("application/json");
        xhr.send(new Blob([data]));
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 400) fn(this);
        }
        xhr.onerror = xhr.error;
        return xhr;
    }

    static error () {
        console.log("client XHR result: " + this.error);
    }
}

export { Xhr };