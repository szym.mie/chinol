### room stage:

0. Waitroom - standard 3 minutes to end.
   If there are 2, 3, 4 ready players the
   game will carry on to stage 1, else
   stage 5.
   
1. Game - players will try to beat the game,
   60 minutes for that max. If there is only
   1 player not asleep the game will end
   displaying 3 stage. The same goes for no 
   players, but instead stage 4 is displayed
   If someone wins stage 2 will be shown.
   
2. Win - somebody has won the game, after
   5 minutes the room is deleted.
   
### player status:

0. Entered a waitroom, not ready
1. Entered a waitroom, ready
2. In the game, waiting for turn
3. In the game, have a turn
4. In the game, lost a turn ( asleep )
5. Outside any game or lobby


8. The
9. same
10. as above
11. But
12. DE-
13. SYNCED

### when a player gets asleep:

If the player doesn't roll the dice, he will 
lose the turn, and be considered asleep.
After the next roll of dice, the status will
be set to normal.

### when a player will get a logoff:

When he wishes for it, or doesn't send a sync
request for:
1. 10 seconds in a waitroom.
2. 30 minutes during a game.
3. 3 hours if free-floating.

So abruptly short waitroom time is so to
prevent flooding.

30 minutes for a game seems reasonable - most
take shorter than 1 hour.

3 hours are so you