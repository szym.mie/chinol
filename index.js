const http = require("http");
const fs = require("fs");
const pg = require("pg");

// load one query from file.
function load_query(path) {
    return fs.readFileSync(path, {encoding: "utf-8"})
        .toString()
        .replace(/\?/g, "$");
}

function load_document(path) {
    return fs.readFileSync(path, {encoding: "utf-8"})
        .toString()
        .replace(/(\n|\r\n)[\s]*/g, "");
}

// mime types conversion.
function mime(path) {
    if (typeof path !== "string") throw TypeError("path arg not of string type.");
    const s = path.split(".");
    switch (s[s.length-1]) {
        case "html": return "text/html";
        case "css": return "text/css";
        case "js": return "text/javascript";
        case "json": return "application/json";
        case "jpg": return "image/jpeg";
        case "svg": return "image/svg+xml";
        default: return "text/html";
    }
}


// serve both get and post.
function serve(req, res) {
    switch (req.method) {
        case "GET":
            get(req, res);
            break;
        case "POST":
            post(req, res);
            break;
        default:
            break;
    }
}


// server static sites and files.
function get(req, res) {
    const pathname = req.url === "/" ? "./static/init.html" : "./static" + req.url;
    console.log("GET -> " + pathname);
    fs.readFile(pathname, (err, data) => {
        if (!err) {
            res.writeHead(200, { "Content-Type": mime(pathname) });
            res.write(data);
        } else {
            res.writeHead(404, "non-existent path.");
        }
        res.end();
    });
}


// respond to ajax.
function post(req, res) {
    let all = "";

    req.on("data", function (data) {
        all += data;
        //console.log(data);
    });

    req.on("end", function () {
        //console.log(all);
        const json = JSON.parse(all);
        switch (req.url) {
            case "/ident":
                requests.ident(json, res);
                break;
            case "/logon":
                requests.logon(json, res);
                break;
            case "/rdy":
                requests.rdy(json, res);
                break;
            case "/act":
                requests.act(json, res);
                break;
            case "/sync":
                requests.sync(json, res);
                break;
            case "/end":
                requests.end(json, res);
                break;
            default:
                break;
        }
    });
}


const nil_uuid = "00000000-0000-0000-0000-000000000000";
const regexp_uuid = /(\d|[a-f]){8}(-(\d|[a-f]){4}){3}-(\d|[a-f]){12}/;


const requests = {
    ident: function (param, res) {
        console.log(param.session === undefined);
        const uuid = param.session === undefined || param.session.match(regexp_uuid) === null ? nil_uuid : param.session;

        postgres
            .query(queries.exists_player, [uuid])
            .then(end => {
                console.log("IDENT -> SESSION: " + param.session);
                res.write(parseInt(end.rows[0].count) ? "/dynamic/room.html" : "/dynamic/logon.html");
                res.end();
            })
            .catch(err => { postgres_error(err); });
    },
    logon: function (param, res) {

        postgres
            .query(queries.BEGIN);

        postgres
            .query(queries.new_player, [param.name])
            .then(end => {
                const session = end.rows[0];
                console.log("LOGON -> SESSION: " + session + ", NAME: " + param.name);

                postgres
                    .query(queries.find_room_empty)
                    .then(end => {
                        const room = end.rows[0];
                        console.log("LOGON -> ROOM: " + room);
                        if (room === undefined) {
                            console.log("LOGON -> NEW");
                            postgres
                                .query(queries.new_room, ["room", session.session])
                                .catch(err => {
                                    postgres_error(err);
                                });
                        } else {
                            console.log("LOGON -> ENTER");
                            postgres
                                .query(queries.enter_room, [room.id, session.session])
                                .catch(err => {
                                    postgres_error(err);
                                });
                        }

                        res.write(JSON.stringify(session));
                        res.end();
                    })
                    .catch(err => { postgres_error(err) });
            })
            .catch(err => { postgres_error(err); });

        postgres
            .query(queries.COMMIT);
    },
    rdy: function (param, res) {
        console.log(param.ready);
        postgres
            .query(queries.change_ready_player, [param.session, param.ready])
            .then(_ => {
                res.write(JSON.stringify({status: "ok"}));
                res.end();
            })
            .catch(err => { postgres_error(err); });
    },
    act: function (param, res) {
        switch (param.no) {
            case 0:
                postgres
                    .query(queries.dice_throw, [param.session])
                    .then(end => {
                        if (end.rows[0] !== undefined)
                            res.write(JSON.stringify({status: "ok", thrown: end.rows[0].last_throw}));
                        else
                            res.write(JSON.stringify({status: "fail"}));
                        res.end();
                    })
                    .catch(err => { postgres_error(err); });
                break;
            case 1:
            case 2:
            case 3:
            case 4:
                postgres
                    .query(queries.pawn_take, [param.session, param.no])
                    .then(_ => {})
                    .catch(err => { postgres_error(err); });
                postgres
                    .query(queries.pawn_select, [param.session, param.no])
                    .then(_ => {
                        res.write(JSON.stringify({status: "ok"}));
                        res.end();
                    })
                    .catch(err => { postgres_error(err); });
                break;
            default:
                res.write(JSON.stringify({status: "errno"}));
                res.end();
                break;
        }
    },
    sync: function (param, res) {
        postgres
            .query(queries.BEGIN);

        console.log("SYNC -> SESSION: ", param.session);

        postgres
            .query(queries.info_about_room, [param.session])
            .then(end => {
                try {
                    res.write(JSON.stringify(end.rows[0]));
                    console.log("SYNC -> DATA");
                } catch {
                    res.write(JSON.stringify({empty: 0}));
                    console.log("SYNC -> EMPTY");
                }
                res.end();
            })
            .catch(err => { postgres_error(err); });

        postgres
            .query(queries.sync_player, [param.session])
            .catch(err => { postgres_error(err); });

        postgres
            .query(queries.COMMIT);
    },
    end: function (param, res) {
        postgres
            .query(queries.delete_player, [param.session])
            .then(_ => {
                res.write(dynamic.end);
                res.end();

                postgres
                    .query(queries.delete_room)
                    .catch(err => { postgres_error(err); });
            })
            .catch(err => { postgres_error(err); });
    }
};


function periodic() {
    console.log("periodic");
    postgres
        .query(queries.BEGIN);

    postgres
        .query(queries.update_player)
        .catch(err => { postgres_error(err); });

    postgres
        .query(queries.delete_room)
        .catch(err => { postgres_error(err); });

    postgres
        .query(queries.update_rooms)
        .catch(err => { postgres_error(err); });

    postgres
        .query(queries.COMMIT);
}


// ---

// const dynamic = {
//     end: load_document("./dynamic/end.html"),
//     logon: load_document("./dynamic/logon.html"),
//     room: load_document("./dynamic/room.html")
// };


// setup possible queries.
const queries = {
    change_ready_player: load_query("./sql/change_ready_player.sql"),
    delete_player: load_query("./sql/delete_player.sql"),
    delete_room: load_query("./sql/delete_room.sql"),
    dice_throw: load_query("./sql/dice_throw.sql"),
    drop_player_table: load_query("./sql/drop_player_table.sql"),
    drop_room_table: load_query("./sql/drop_room_table.sql"),
    enter_room: load_query("./sql/enter_room.sql"),
    exists_player: load_query("./sql/exists_player.sql"),
    update_player: load_query("./sql/update_players.sql"),
    update_rooms: load_query("./sql/update_rooms.sql"),
    find_room_by_name: load_query("./sql/find_room_by_name.sql"),
    find_room_empty: load_query("./sql/find_room_empty.sql"),
    info_about_player: load_query("./sql/info_about_player.sql"),
    info_about_room: load_query("./sql/info_about_room.sql"),
    init_player_table: load_query("./sql/init_player_table.sql"),
    init_room_table: load_query("./sql/init_room_table.sql"),
    new_player: load_query("./sql/new_player.sql"),
    new_room: load_query("./sql/new_room.sql"),
    pawn_select: load_query("./sql/pawn_select.sql"),
    pawn_take: load_query("./sql/pawn_take.sql"),
    room_of_player: load_query("./sql/room_of_player.sql"),
    sync_player: load_query("./sql/sync_player.sql"),

    BEGIN: "BEGIN;",
    COMMIT: "COMMIT;",
    ROLLBACK: "ROLLBACK;"
};

// setup http client.
const http_server = http.createServer(serve);
http_server.listen(process.env.PORT);

// setup heroku postgres client.
console.log(process.env.DATABASE_URL);
const postgres = new pg.Client({
    connectionString: process.env.DATABASE_URL,
    ssl: {
        sslmode: "require",
        rejectUnauthorized: false
    }
});
// setup localhost client.
// const postgres = new pg.Client({
//     user: "postgres",
//     host: "localhost",
//     database: "postgres",
//     password: "passwd",
//     port: 5432,
// });


postgres_init().catch(err => { postgres_error(err) });

async function postgres_init() {
    await postgres
        .connect()
        .then(() => { console.log("postgres CONNECTED."); })
        .catch(err => { postgres_error(err) });

    await postgres.query(queries.drop_room_table);
    await postgres.query(queries.drop_player_table);

    await postgres.query(queries.init_player_table);
    await postgres.query(queries.init_room_table);

    // do some operations on the database once about 1 second.
    setInterval(periodic, 1000);
}

function postgres_error(err) {
    console.log("postgres FATAL: " + err.stack);
    postgres.query(queries.ROLLBACK);
}